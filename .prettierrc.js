module.exports = {
  $schema: 'http://json.schemastore.org/prettierrc',
  bracketSpacing: true,
  endOfLine: 'lf',
  jsxBracketSameLine: false,
  printWidth: 120,
  singleQuote: true,
  tabWidth: 2,
  semi: false,
  trailingComma: 'es5',
}
