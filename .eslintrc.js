module.exports = {
  root: true,
  extends: ['react-app', 'react-app/jest', '@react-native-community', 'prettier', 'prettier/react'],
  plugins: ['prettier', '@typescript-eslint'],
  rules: {
    'prettier/prettier': 'error',
    'no-console': 'warn',
    'react-hooks/exhaustive-deps': 'off',
  },
}
