import React from 'react'
import { View, Text, Image } from 'react-native'
import './App.css'
import hero from './hero.png'

function App() {
  return (
    <View>
      <Text>Hello World!</Text>
      <Image style={{ width: 400, height: 400, backgroundColor: 'red' }} source={{ uri: hero }} />
    </View>
  )
}

export default App
